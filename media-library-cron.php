<?php
/*
Plugin Name: UCToday Media Accessibility Check
Description: Checks all media objects in the library for accessibility concerns on a daily basis.
Version: 1.0.1
Author: Brian Kelleher
License: UNLICENSED
*/

register_activation_hook(__FILE__, 'media_accessibilty_activation');
add_action('media_library_accessibility_check', 'media_accessibility_cron');


function media_accessibilty_activation() {
    wp_schedule_event(time(), 'daily', 'media_library_accessibility_check');
}

register_deactivation_hook(__FILE__, 'media_accessibility_deactivation');

function media_accessibility_deactivation() {
    wp_clear_scheduled_hook('media_library_accessibility_check');
}


function media_accessibility_cron() {

    // 1. Count the number of attachment records
    // 2. Do the media evaluation on ~100 records at a time.
    // 3. Unset the current media library each time.

    global $wpdb;

    $attachment_count_result = $wpdb->get_results( 'SELECT COUNT(CASE WHEN post_type = "attachment" then 1 else null end) FROM wp_posts', ARRAY_N );

    // Count the number of attachment records
    $total_num_attachments = (integer) $attachment_count_result[0][0];

    // Number of attachments to handle per request
    $posts_per = 100;

    // The max page number we must reach to handle all attachments.
    $max_pages = ceil( $total_num_attachments / $posts_per );

    $rows_affected = 0;

    // Empty the current database set.
    $wpdb->query( 'TRUNCATE TABLE wp_media_cron' );

    // Perform operations for each "page" of attachments.
    for ( $i = 0; $i < $max_pages; $i++ ) {

        // Our array to hold all media with missing meta
        // array['author_id']['media_id'];
        $broken_media = array();

        // Update offset for this set.
        $off = (integer) ($i * $posts_per);

        $args = array(
            'post_type'         => 'attachment',
            'posts_per_page'    => $posts_per,
            'offset'            => $off,
            'fields'            => 'ids',
            'post_mime_type'   => array( 'image/bmp', 'image/gif', 'image/x-icon', 'image/jpeg', 'image/png', 'image/tiff' ),
        );

        // Call to database, returns array of WP_POST objects
        $media_library = get_posts( $args );


        // !empty( $media_library )
        if ( !empty( $media_library ) ) {
            // we're good to go
            foreach ( $media_library as $mediaID ) {

                $media_obj = get_post( $mediaID );

                // Manually get caption / alt tag
                $caption = $media_obj->post_excerpt;
                $alt = get_post_meta( $mediaID, '_wp_attachment_image_alt', true );

                if( !$caption || !$alt ) {
                // Perform if caption or alt is missing or empty.

                    $author_id = $media_obj->post_author;

                    // Memory concerns
                    unset( $media_obj );

                    if( $author_id ) {
                        if( array_key_exists( $author_id, $broken_media ) ) {
                            // Add to author's key in $broken_media
                            array_push( $broken_media[$author_id],  $mediaID );
                        } else {
                            // Create author's key in $broken_media and add to it
                            $broken_media[$author_id] = array( $mediaID );
                        }
                    }

                }

            }

            // Memory concerns
            unset( $media_library );

        }


        if ( !empty( $broken_media ) ) {
        // Perform saving to the database, if there are broken media.

            $values = array();

            // Loop through all problem media
            foreach ( $broken_media as $author => $attachments ) {

                foreach ( $attachments as $attach ) {
                    // Build array with queries
                    $values[] = $wpdb->prepare( "( %d, %d )", $author, $attach );
                }

            }

            // Memory concerns
            unset( $broken_media );

            // Build query from all prepared statements
            $query = "INSERT INTO wp_media_cron ( post_author, attachment_id ) VALUES ";
            $query .= implode( ",\n", $values );

            // Memory concerns
            unset( $values );

            // Finally run the query.
            $results = $wpdb->query( $query );

            if ( false !== $results ) {
                $rows_affected += $results;
            }

            // Memory concerns.
            unset( $results );
            unset( $query );

        }

    }

    mail( "bk@uconn.edu", "Media Cron Job", "You affected $rows_affected rows.");

}


/*
Add Widget to Dashboard
*/
function media_accessibility_add_dashboard() {

    wp_add_dashboard_widget(
        'media_accessibility_widget',
        'Media Accessibility',
        'media_accessibility_dashboard_cb'
    );

}
add_action( 'wp_dashboard_setup', 'media_accessibility_add_dashboard' );

/*
Callback function to show widget
*/
function media_accessibility_dashboard_cb() {

    // Identify current user
    $current_id = get_current_user_id();

    global $wpdb;

    // Select multiple for the current logged in user.
    $user_affected_media = $wpdb->get_results( $wpdb->prepare(
        "SELECT post_author, attachment_id FROM wp_media_cron WHERE post_author = %d",
        $current_id
    ), ARRAY_A );


    if ( $user_affected_media ) {

        $media_count = count( $user_affected_media );

        // Echo affected media for that user.
        echo "Media Accessibility Concerns: $media_count \n";
        echo "<p>Please make sure each of the below items has an 'Alt Tag' and 'Caption'.</p>";

        // Start the list of affected media.
        $list = '<ol>';

        foreach ( $user_affected_media as $attachment ) {

            // Get attachment object
            $attachment_obj = get_post( $attachment['attachment_id'] );

            // Create URL that points them directly to a page that can edit the metadata.
            $base_url = admin_url('upload.php?item=' . $attachment_obj->ID );

            $list .= '<li><a style="color: red;" href="' . $base_url . '">' . $attachment_obj->post_title . '</a></li>';
        }

        $list .= '</ol>';

        echo $list;

    } else {

        echo "You have no Media Accessibility Concerns!";

    }

}